package com.itt.main;

import com.itt.pojo.Output;
import com.itt.pojo.Payload;
import com.itt.run.APIExecutor;
import com.itt.run.CodeParser;

public class Main {

	public static void main(String[] args) 
	{
		Payload payload = getDetailsFromFrontend();
		
		APIExecutor apiExecutor = new APIExecutor();
		Output output = apiExecutor.executeApi(payload);
		
		
		System.out.println("Output : "+output.getOutput());
	}

	private static Payload getDetailsFromFrontend() 
	{
		String fileName ="Code.txt";
		
		String script = CodeParser.parseCode(fileName);
		String userEnteredStdin = "";	//Not test with custom input
		String language = "java";
		String versionIndex = "0";
		
		Payload payload = preparePayload(script,userEnteredStdin,language,versionIndex);
		return payload;
	}

	private static Payload preparePayload(String script, String userEnteredStdin, String language, String versionIndex) 
	{
		Payload payload = new Payload();
		payload.setScript(script);
		payload.setLanguage(language);
		payload.setVersionIndex(versionIndex);
		
		if(userEnteredStdin.equals(""))
		{
			payload.setStdin(getDefaultTestCase());
		}
		else
		{
			payload.setStdin(userEnteredStdin);
		}
		
		return payload;
	}

	private static String getDefaultTestCase() 
	{
		String defaultTestCase="4 34 32 45 23";
		return defaultTestCase;
	}
}
