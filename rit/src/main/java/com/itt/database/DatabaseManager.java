package com.itt.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseManager 
{
	Connection connection;
	Statement statement;
	
	public DatabaseManager()
	{
		try 
		{
			connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/imq-ashrith","root","root");  
			statement=connection.createStatement();  
			//ResultSet rs=stmt.executeQuery("select * from emp");
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}    
	}
}
