package com.itt.run;

import static com.itt.constants.Constants.*;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itt.pojo.Output;
import com.itt.pojo.Payload;

public class APIExecutor 
{
	ObjectMapper objectMapper = new ObjectMapper();
	
	public Output executeApi(Payload payload)
	{
		URL url;
		StringBuilder response;
		Output output = null;
		
		try 
		{
			url = new URL (EXECUTE_API);
			HttpURLConnection con = (HttpURLConnection)url.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json; utf-8");
			con.setRequestProperty("Accept", "application/json");
			con.setDoOutput(true);
			
			String jsonInputString = prepareJsonInputString(payload);
			
			try(OutputStream os = con.getOutputStream()) 
			{
			    byte[] input = jsonInputString.getBytes("utf-8");
			    os.write(input, 0, input.length);			
			}
			
			try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) 
			{
				response = new StringBuilder();
				String responseLine = null;
				while ((responseLine = br.readLine()) != null) 
				{
					response.append(responseLine.trim());
				}
				
				output = objectMapper.readValue(response.toString(), Output.class);
			}
			
			return output;
		} 
		catch (MalformedURLException e) 
		{
			return output;
			
		} 
		catch (IOException e) 
		{
			return output;
		}
	}

	private String prepareJsonInputString(Payload payload) 
	{
		StringBuffer buffer=new StringBuffer();  
		String jsonInputString=buffer.append("{")
				.append(" \"clientId\":")
				.append(INVERTED_COMMAS).append(CLIENT_ID).append(INVERTED_COMMAS).append(COMMA)
				.append(" \"clientSecret\": ")
				.append(INVERTED_COMMAS).append(CLIENT_SECRET).append(INVERTED_COMMAS).append(COMMA)
				.append(" \"script\": ")
				.append(INVERTED_COMMAS).append(payload.getScript()).append(INVERTED_COMMAS).append(COMMA)
				.append(" \"stdin\": ")
				.append(INVERTED_COMMAS).append(payload.getStdin()).append(INVERTED_COMMAS).append(COMMA)
				.append(" \"language\": ")
				.append(INVERTED_COMMAS).append(payload.getLanguage()).append(INVERTED_COMMAS).append(COMMA)
				.append(" \"versionIndex\": ")
				.append(INVERTED_COMMAS).append(payload.getVersionIndex()).append(INVERTED_COMMAS)
				.append("}").toString();
		
		System.out.println("Test cases :\n"+payload.getStdin());
		
		return jsonInputString;
	}
}
