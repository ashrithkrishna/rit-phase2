package com.itt.run;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * This class takes file containing source code as input and produces single line output
 * for the code.
 * 
 * @author ashrith.krishna
 * @author yash.maheshwari
 *
 */
public class CodeParser {
	
	private static String tabCharacter = "\t";
	private static String doubleQuoteCharacter = "\"";
	private static String newLineCharacterJson = "\\n";
	private static String tabCharacterJson = "\\t";
	private static String doubleQuoteCharacterJson = "\\\"";
	
	public static String parseCode(String fileName) {
		
		StringBuilder output = new StringBuilder();
		File sourceFile = new File(fileName);
		String fileNotFoundMessage = "File does not exists. Exiting...";
		
		if (sourceFile.exists()) {
			try (BufferedReader fileReader = new BufferedReader(new FileReader(sourceFile))) {
				String line = "";
				while((line = fileReader.readLine()) != null) {
//					if (line.contains(comment)) {
//						int commentPosition = line.indexOf(comment, 0);
//						line = line.substring(0, commentPosition);
//					}
					line = line.replace(doubleQuoteCharacter, doubleQuoteCharacterJson);
					line = line.replace(tabCharacter, tabCharacterJson);
					output.append(line).append(newLineCharacterJson);
				}
				
			} 
			catch (IOException e) {
				e.printStackTrace();
			}
		} 
		else 
		{
			System.out.println(fileNotFoundMessage);
			System.exit(0);
		}
		
		return output.toString();
	}
}
