package com.itt.pojo;

public class Payload 
{
	private String script;
	private String stdin;
	private String language;
	private String versionIndex;
	
	public Payload() {}

	public Payload(String clientId, String clientSecret, String script, String stdin, String language,
			String versionIndex) {

		this.script = script;
		this.stdin = stdin;
		this.language = language;
		this.versionIndex = versionIndex;
	}

	public String getScript() {
		return script;
	}

	public void setScript(String script) {
		this.script = script;
	}

	public String getStdin() {
		return stdin;
	}

	public void setStdin(String stdin) {
		this.stdin = stdin;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getVersionIndex() {
		return versionIndex;
	}

	public void setVersionIndex(String versionIndex) {
		this.versionIndex = versionIndex;
	}	
}
