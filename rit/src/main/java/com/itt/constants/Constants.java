package com.itt.constants;

public final class Constants 
{
	private Constants(){}
	
	public static final String CREDIT_SPENT_API="https://api.jdoodle.com/v1/credit-spent";
	public static final String EXECUTE_API="https://api.jdoodle.com/v1/execute";
	
	public static final String CLIENT_ID="5fd980b267060900de011f896b830b9f";
	public static final String CLIENT_SECRET="789a0ce7b615eb40a65cccea98afc822e6dfb92f9d74ca5421844d466b5085d0";
	
	public static final String INVERTED_COMMAS="\"";
	public static final String COMMA=",";

}
