import java.
//This is the program to find 3rd largest in an array
class ThirdLargestNumberinArray
{  
	public static int getThirdLargest(int[] a, int total){  
		int temp;  
		for (int i = 0; i < total; i++)   
		{  
          for (int j = i + 1; j < total; j++)   
          {  
              if (a[i] > a[j])   
              {  
                  temp = a[i];  
                  a[i] = a[j];  
                  a[j] = temp;  
              }  
          }  
      }  
     return a[total-3];  
	}
}

public class Main
{
	public static void main(String args[])
	{  
		Scanner scan = new Scanner(System.in);
		ThirdLargestNumberinArray tLNA=new ThirdLargestNumberinArray();
		System.out.println("Enter the size of array :");
		int n = scan.nextInt();
		System.out.println(n);
		int a[] = new int[n];  
		System.out.println("Enter array elements :");
		
		for(int i=0;i<n;i++)
		{
			a[i]=scan.nextInt();
		}  
		
		System.out.println("Third Largest: "+tLNA.getThirdLargest(a,n)); 
	}
}